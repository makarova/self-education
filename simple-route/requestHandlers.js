function hello() {
    console.log("Request handler 'hello' was called.");
    return "Hello";
}

function bye() {
    console.log("Request handler 'bye' was called.");
    return "Bye";
}

function home() {
    console.log("Request handler 'home' was called.");
    return "Home";
}

exports.home = home;
exports.hello = hello;
exports.bye = bye;