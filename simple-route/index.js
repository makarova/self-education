var server = require("./server");
var router = require("./router");
var requestHandlers = require("./requestHandlers");

var handle = {}
handle["/"] = requestHandlers.home;
handle["/hello"] = requestHandlers.hello;
handle["/bye"] = requestHandlers.bye;


server.start(router.route, handle);
