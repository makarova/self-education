function hello(response, query) {
    console.log("Request handler 'hello' was called.");

    var name = query['name'] || '';

    response.writeHead(200, {"Content-Type": "text/html"});
    response.write("Hello " + name);
    response.end();
}

function home(response) {
    console.log("Request handler 'home' was called.");
    response.writeHead(200, {"Content-Type": "text/html"});
    response.write("Homepage");
    response.end();
}

exports.home = home;
exports.hello = hello;
