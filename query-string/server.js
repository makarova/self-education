var http = require("http");
var url = require("url");
var querystring = require('querystring');


function start(route, handle) {
    function onRequest(request, response) {
        var urlParts = url.parse(request.url);
        var query = querystring.parse(urlParts.query);

        console.log("Request for " + urlParts.pathname + " received.");

        request.setEncoding("utf8");

        request.addListener("data", function(postDataChunk) {
            postData += postDataChunk;
            console.log("Received POST data chunk '"+
            postDataChunk + "'.");
        });

        request.addListener("end", function() {
            route(handle, urlParts.pathname, response, query);
        });
    }

    http.createServer(onRequest).listen(1337);
    console.log("Server has started.");
}

exports.start = start;